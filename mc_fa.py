import sys

import numpy  as np

from tqdm import *

from blackjack import Blackjack
from blackjack_biased import Blackjack_Biased
from utils import save_data, load_data, Stats, QValue, to_matrix, min_max_scaler

from matplotlib import pyplot as plt

SHUFFLE = 1000
GAMMA = 1

N0 = 100
HITS = Blackjack.HITS #0
STICK = Blackjack.HITS #1
USE_ACE = Blackjack.USE_ACE #2
IDLE_ACE = Blackjack.IDLE_ACE #3

DEFAULT_EPISODES = 3

#Number of episodes
EPISODES = int(sys.argv[1]) if len(sys.argv) > 1 else DEFAULT_EPISODES
use_bias_env = True if len(sys.argv) > 2 else False
W = []

#initialize states [dealer_top_card, player_hand, usable_ace, actions]
#actions = 0 - HITS, 1 - STICK
#0 element not used
if use_bias_env:
   print('Initializing states with %s_%s.pickle' %(sys.argv[2], sys.argv[3]))
   W = [] # load from file
else:
   #initialize the weights
   #4 actions each, action 3 states + 1 intercept
   W = np.random.rand(4, 4)

print('EPISODES: %d ' %EPISODES)
print('Biased environment: ', use_bias_env)

def policy(st, W, stats):
   mod_st = np.array(min_max_scaler(st) + (1,))
   actions = np.dot(W, mod_st.T)
   if np.random.uniform() > stats.epsilon(st):
      return np.argmax(actions)
   return np.random.randint(low=HITS, high=(IDLE_ACE + 1))

def update(st, ac, g, W, stats):
   mod_st = np.array(min_max_scaler(st) + (1,))
   err = g - np.dot(W[ac], mod_st.T)
   del_W = stats.alpha(st, ac) * err * mod_st.T
   W[ac] += del_W

win = 0
lose = 0
draw = 0
has_ace = 0

stats = Stats()
stats.reset_traces()

for i in tqdm(range(EPISODES)):
   episode = []
   blackjack = Blackjack(SHUFFLE)
   state = blackjack.reset()
   terminate = False

   while not terminate:
      action = policy(state, W, stats)

      stats.update_stats(state, action)

      new_state, reward, terminate = blackjack.step(action)

      episode.append((state, action, reward, new_state))

      state = new_state

   has_ace += 1 if blackjack.has_usable_ace() else 0

   _, _, outcome, _ = episode[-1]

   if 1 == outcome:
      win += 1
   elif -1 == outcome:
      lose += 1
   else:
      draw += 1

   for i, v in enumerate(episode):
      state, action, reward, _ = v
      gain = sum([e[2] for e in episode[i:]])
      update(state, action, gain, W, stats)

print('win: %d, lose: %d, draw: %d' %(win, lose, draw))
print('win: %0.1f%%, lose: %0.1f%%, draw: %0.1f%%' %(win/EPISODES * 100, lose/EPISODES * 100, draw/EPISODES * 100))
print('usable ace: %0.2f%%' %(has_ace/EPISODES * 100))

fn = save_data(data=W \
      , prefix='montecarlo-approx' + ('-biased' if use_bias_env else ''), iterations=EPISODES)
print('Saved %s' %fn)

